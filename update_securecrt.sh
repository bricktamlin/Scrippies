#!/bin/sh

##############################
#
#     Provided as is but you can ask me questions if you want on slack
#
#     Simple Bash script for creating import files for SecureCRT
#
#     Requires SecureCRT 7.2 or later
#
##############################


if [ -f ./masterlist.txt ]; then
	echo "Backing up previous master list"
	mv masterlist.txt masterlist.txt.`date +%s`
else
	echo "No master file found, creating header and file now"
fi
sleep 2

echo "folder,username,hostname,protocol,session_name" >>masterlist.txt

echo UPDATING SITE 1
curl -s serverlist | grep splunk-site1 |tr ' ' '\n'|sed 's/^splunk-site1:$//g'| sed '/^$/d' >>site1
for i in `cat ./site1`; do echo Splunk/Indexers/Site1,centos,$i,SSH2,$i; done >>scrt_site1.txt
rm site1

echo UPDATING SITE 2
curl -s serverlist | grep splunk-site2 |tr ' ' '\n'|sed 's/^splunk-site2:$//g'| sed '/^$/d' >>site2
for i in `cat ./site2`; do echo Splunk/Indexers/Site2,centos,$i,SSH2,$i; done >>scrt_site2.txt
rm site2

echo UPDATING SITE 3
curl -s serverlist | grep splunk-site3 |tr ' ' '\n'|sed 's/^splunk-site3:$//g'| sed '/^$/d' >>site3
for i in `cat ./site3`; do echo Splunk/Indexers/Site3,centos,$i,SSH2,$i; done >>scrt_site3.txt
rm site3

echo "UPDATING SHC (Core)"
curl -s serverlist | grep splunk-sh |tr ' ' '\n'|sed 's/^splunk-sh:$//g'| sed '/^$/d' >>shc
for i in `cat ./shc`; do echo Splunk/SHC/Core,centos,$i,SSH2,$i; done >>scrt_shc.txt
rm shc

echo "UPDATING SHC (ITSI)"
curl -s serverlist | grep splunk-itsi-sh |tr ' ' '\n'|sed 's/^splunk-itsi-sh:$//g'| sed '/^$/d' >>itsi
for i in `cat ./itsi`; do echo Splunk/SHC/ITSI,centos,$i,SSH2,$i; done >>scrt_shc.txt
rm itsi

echo "UPDATING Splunk MGT"
curl -s serverlist | grep splunk-mgmt|tr ' ' '\n'|sed 's/^splunk-mgmt:$//g'| sed '/^$/d' >>splunk-mgmt
for i in `cat ./splunk-mgmt`; do echo Splunk/MGT,centos,$i,SSH2,$i; done >>scrt_splunkmgmt.txt
rm splunk-mgmt


echo "Creating Master list for SecureCRT import"
cat scrt_*.txt >>masterlist.txt
rm scrt_*.txt
